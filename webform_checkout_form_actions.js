Drupal.behaviors.webform_CheckoutBehavior = function (context) {

$("#edit-submitted-competitor-details-date-of-birth-year").change(function(){

		var year = $("#edit-submitted-competitor-details-date-of-birth-year option:selected").val();
		
		  // Youth C (born 1999-1998)
// 		  Youth B (born 1997-1996)
// 		  Youth A (born 1995-1994)
// 		  Junior (born 1993-1992)
// 		  Senior (born 1991-1967)
// 		  Veteran (born 1966 or earlier)
		
		switch(true)
        {
            case ((year == 1998) || (year == 1999)): 
            	SetRadio("Youth C (born 1999-1998)");SetCost(20);
            	break;
			case ((year == 1997) || (year == 1996)): 
				SetRadio("Youth B (born 1997-1996)");SetCost(20);
				break;
			case ((year == 1995) || (year == 1994)): 
				SetRadio("Youth A (born 1995-1994)");SetCost(20);
				break;
			case ((year == 1993) || (year == 1992)): 
				SetRadio("Junior (born 1993-1992)");SetCost(20);
				break;
			case ((year <= 1991) && (year >= 1967)): 
				SetRadio("Senior (born 1991-1967)");SetCost(25);
				break;
			case (year <= 1966): 
				SetRadio("Veteran (born 1966 or earlier)");SetCost(25);
				break;
            default: caption ="default";
        }
	});
};

Date.prototype.age=function(at){
    var value = new Date(this.getTime());
    var age = at.getFullYear() - value.getFullYear();
    value = value.setFullYear(at.getFullYear());
    if (at < value) --age;
    return age;
};

function SetCost(input){
	var radio = $('input[name="submitted[categories][cost]"]:radio').filter('[value='+input+']').attr("checked","checked");
}

function SetRadio(target) {
	var radios = $('input[name="submitted[categories][competitor_category]"]:radio');
	$.each(radios,function(index,value){
		if ($.trim(value.nextSibling.nodeValue)==$.trim(target))
		{$(value).attr("checked","checked");}
	})
}