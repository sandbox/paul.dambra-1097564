Drupal.behaviors.webform_CheckoutBehavior = function (context) {
	//stick corners on stuff
	$(".linker-rounding").corner();
	$(".holder").corner();
	$(".link-item").corner();
	
	//show delete.png when mouse over
	$(".holder").mouseover(
		function() {
			$(this).children("img").removeClass('hide');
		}
	);
	//and hide when mouseout
	$(".holder").mouseout(
		function() {
			$(this).children("img").addClass('hide');
		}
	);
	
	//ajax call when delete.png clicked
	$(".delete").click(
		function(){
			var ids = $(this).attr('id').split("-");
			alert($(this).parent().attr("class"));
			if ($(this).parent().attr("class").indexOf("item")!=-1){
				var callbackUrl = "/webform_checkout/remove-component/"+ids[0]+"/"+ids[1];
			}
			else {
				var callbackUrl = "/webform_checkout/remove-quantity/"+ids[0]+"/"+ids[1];
			}
			$.ajax({
				type: 'POST',
				url:callbackUrl,
				success:function(){location.reload();}
				});
		}
	);
};